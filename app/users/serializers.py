from rest_framework import serializers

from .models import User

USER_FIELDS = ("email", "created",)


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = USER_FIELDS + ("password",)
        extra_kwargs = {
            "created": {"read_only": True},
        }
