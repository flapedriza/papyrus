from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from .serializers import UserSerializer


class SignUpView(APIView):

    serializer_class = UserSerializer
    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        data = request.data
        serializer = self.serializer_class(data=data)

        if serializer.is_valid():
            serializer.save()

            user = serializer.instance
            user.set_password(data['password'])
            user.save()

            token = Token.objects.create(user=user)
            result = {'token': token.key}

            return Response(result, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LoginView(APIView):

    serializer_class = AuthTokenSerializer
    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            user = serializer.validated_data['user']
            token, created = Token.objects.get_or_create(user=user)
            result = {'token': token.key}
            response_status = status.HTTP_201_CREATED if created else status.HTTP_200_OK
            return Response(result, status=response_status)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
