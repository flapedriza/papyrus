from __future__ import unicode_literals

import uuid

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from rest_framework.authtoken.models import Token

from .managers import DefaultUserManager


class User(AbstractBaseUser, PermissionsMixin):

    USERNAME_FIELD = "email"

    objects = DefaultUserManager()

    uuid = models.UUIDField(primary_key=True, editable=False, blank=True,
                            default=uuid.uuid4)
    email = models.EmailField('Email address', unique=True)

    is_staff = models.BooleanField('staff', default=False)
    is_active = models.BooleanField('active', default=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):  # pragma: no cover
        return "[{}] {} {}".format(self.email, self.first_name, self.last_name)

    @property
    def token(self):
        token = Token.objects.get(user=self)
        return token

    @property
    def first_name(self):
        return self.email

    @property
    def last_name(self):
        return self.email

    def get_short_name(self):
        return self.email
