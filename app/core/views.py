from rest_framework import mixins, viewsets, status
from rest_framework.decorators import detail_route
from rest_framework.exceptions import NotFound
from rest_framework.response import Response

from django_filters.rest_framework import DjangoFilterBackend

from app.core.models import Todo, Project, Tag
from app.core.serializers import TodoSerializer, ProjectSerializer, TagSerializer

from .filters import TodoFilter


class TodoViewset(viewsets.ModelViewSet):
    """
    """
    serializer_class = TodoSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = TodoFilter
    lookup_field = "number"

    def get_queryset(self):
        return Todo.objects.filter(user=self.request.user)

    @detail_route(methods=["post"])
    def done(self, request, number=None):
        try:
            todo = self.get_queryset().get(number=number)
        except Todo.DoesNotExist:
            raise NotFound

        todo.done = True
        todo.save()

        return Response({"Todo %d done" % todo.pk}, status.HTTP_200_OK)

    @detail_route(methods=["post"])
    def undone(self, request, number=None):
        try:
            todo = self.get_queryset().get(number=number)
        except Todo.DoesNotExist:
            raise NotFound

        todo.done = False
        todo.save()

        return Response({"Todo %d undone" % todo.pk}, status.HTTP_200_OK)


class TagViewSet(
    mixins.ListModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin,
    viewsets.GenericViewSet
):
    """
    Viewset to list, get, update or delete a Tag
    """
    serializer_class = TagSerializer
    lookup_field = "number"

    def get_queryset(self):
        return Tag.objects.filter(user=self.request.user)


class ProjectViewSet(viewsets.ModelViewSet):
    """
    Viewset to list, get, create, update and delete a Project
    """
    serializer_class = ProjectSerializer

    def get_queryset(self):
        return Project.objects.filter(users__in=[self.request.user])
