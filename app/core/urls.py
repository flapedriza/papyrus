from rest_framework import routers

from app.core.views import TodoViewset, TagViewSet, ProjectViewSet

router = routers.SimpleRouter()
router.register(r"todos", TodoViewset, base_name="todos")
router.register(r"tags", TagViewSet, base_name="tags")
router.register(r"projects", ProjectViewSet, base_name="projects")

urlpatterns = router.urls
