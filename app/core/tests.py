from django.core.urlresolvers import reverse
from django.test import TestCase

from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient, APITestCase

from app.users.models import User
from .models import Todo, Tag


class CreateUserMixin(object):

    def create_user(self, email, password, is_superuser=False):
        user = User.objects.create(email=email, is_superuser=is_superuser)
        user.set_password(password)
        user.save()
        Token.objects.create(user=user)
        return user

    def create_authenticated_api_client(self, token):
        api = APIClient()
        api.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        return api


class TodoModelTest(TestCase, CreateUserMixin):

    def setUp(self):
        self.user = self.create_user("foo@foo.com", "foo")
        self.user2 = self.create_user("foo2@foo.com", "foo")

    def test_todos_autoincrement_number(self):
        self.assertEqual(
            Todo.objects.create(description="bla", user=self.user).number, 1)
        self.assertEqual(
            Todo.objects.create(description="ble", user=self.user).number, 2)

        self.assertEqual(
            Todo.objects.create(description="bla", user=self.user2).number, 1)
        self.assertEqual(
            Todo.objects.create(description="ble", user=self.user2).number, 2)

    def test_todos_ordering_by_priority(self):
        Todo.objects.create(user=self.user, priority=2, description="foo2")
        Todo.objects.create(user=self.user, priority=1, description="foo")

        todos = Todo.objects.filter(user=self.user)

        self.assertEqual(todos[0].priority, 1)
        self.assertEqual(todos[1].priority, 2)


class TagModelTest(TestCase, CreateUserMixin):

    def setUp(self):
        self.user = self.create_user("foo@foo.com", "foo")
        self.user2 = self.create_user("foo2@foo.com", "foo")

    def test_tag_autoincrement_number(self):
        self.assertEqual(
            Tag.objects.create(name="bla", user=self.user).number, 1)
        self.assertEqual(
            Tag.objects.create(name="ble", user=self.user).number, 2)

        self.assertEqual(
            Tag.objects.create(name="bla", user=self.user2).number, 1)
        self.assertEqual(
            Tag.objects.create(name="ble", user=self.user2).number, 2)


class TodoViewSetTest(APITestCase, CreateUserMixin):

    def setUp(self):
        self.email = 'test@test.com'
        self.password = 'testpass'
        self.user = self.create_user(email=self.email, password=self.password)
        self.token = self.user.token
        self.api = self.create_authenticated_api_client(self.user.token)

    def test_todos_endpoint_requires_auth(self):
        api = APIClient()
        response = api.post(reverse("todos-list"), {"foo": "foo"}, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = api.get(reverse("todos-list"))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_todos_queryset_depends_on_user(self):
        Todo.objects.create(user=self.user, description="todo user 1")

        user2 = self.create_user("foo@foo.com", "foo")
        Todo.objects.create(user=user2, description="todo user 2")

        response = self.api.get(reverse("todos-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["description"], "todo user 1")

    def test_todos_api_list(self):
        response = self.api.get(reverse("todos-list"), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("count"), 0)
        self.assertEqual(response.data.get("results"), [])

        # Create one todo for current user
        Todo.objects.create(user=self.user, description="todo %s" % self.user.email)

        response = self.api.get(reverse("todos-list"), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("count"), 1)
        self.assertEqual(len(response.data.get("results")), 1)
        self.assertEqual(
            response.data.get("results")[0]["description"],
            "todo %s" % self.user.email)

        # Create one todo for other user, and validate that is not returned
        Todo.objects.create(user=User.objects.create(email="other@test.com"))

        response = self.api.get(reverse("todos-list"), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("count"), 1)
        self.assertEqual(len(response.data.get("results")), 1)
        self.assertEqual(
            response.data.get("results")[0]["description"],
            "todo %s" % self.user.email)

    def test_todos_api_create(self):
        payload = {
            "description": "foo description",
        }
        response = self.api.post(reverse("todos-list"), payload, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Todo.objects.count(), 1)

        todo = Todo.objects.latest("created")

        self.assertEqual(todo.description, "foo description")
        self.assertEqual(todo.done, False)

    def test_todos_api_done(self):
        todo = Todo.objects.create(user=self.user)
        self.assertFalse(todo.done)

        response = self.api.post(reverse("todos-done", args=[todo.number]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        todo = Todo.objects.get(number=todo.number)
        self.assertTrue(todo.done)

        # Try to done a fake task
        response = self.api.post(reverse("todos-done", args=[99]))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_todos_api_undone(self):
        todo = Todo.objects.create(user=self.user, done=True)
        self.assertTrue(todo.done)

        response = self.api.post(reverse("todos-undone", args=[todo.number]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        todo = Todo.objects.get(number=todo.number)
        self.assertFalse(todo.done)

        # Try to udone a fake task
        response = self.api.post(reverse("todos-undone", args=[99]))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_todos_api_detail(self):
        todo = Todo.objects.create(user=self.user, description="foo description")

        response = self.api.get(reverse("todos-detail", args=[todo.number]))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["description"], "foo description")
        self.assertFalse(response.data["done"])

    def test_todos_api_full_update(self):
        todo = Todo.objects.create(user=self.user, description="foo description")

        # Update a task of current user
        payload = {"description": "foo updated", "done": True}
        response = self.api.put(
            reverse("todos-detail", args=[todo.number]), payload,
            format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        todo = Todo.objects.get(number=todo.number)
        self.assertEqual(todo.description, "foo updated")
        self.assertTrue(todo.done)

    def test_todos_api_partial_update(self):
        todo = Todo.objects.create(user=self.user, description="foo description")

        # Update a task of current user
        payload = {"description": "foo updated"}
        response = self.api.patch(
            reverse("todos-detail", args=[todo.number]), payload,
            format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        todo = Todo.objects.get(number=todo.number)
        self.assertEqual(todo.description, "foo updated")

    def test_todos_api_delete(self):
        todo = Todo.objects.create(user=self.user, description="foo description")
        response = self.api.delete(reverse("todos-detail", args=[todo.number]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            Todo.objects.filter(number=todo.number, user=self.user).count(), 0)

    def test_todos_api_create_with_tags(self):
        payload = {
            "description": "foo description",
            "tags": [
                {"name": "tag2", "color": "red"},
            ]
        }

        response = self.api.post(reverse("todos-list"), payload, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        todo = Todo.objects.latest("created")
        self.assertEqual(todo.description, "foo description")
        self.assertEqual(todo.tags.count(), 1)

        self.assertEqual(todo.tags.all()[0].user, todo.user)
        self.assertEqual(todo.tags.all()[0].name, "tag2")
        self.assertEqual(todo.tags.all()[0].color, "red")

        # Test that tags aren't created again if already created
        payload = {
            "description": "foo description",
            "tags": [
                {"name": "tag2", "color": "red"},
            ]
        }
        self.assertEqual(Tag.objects.all().count(), 1)

        response = self.api.post(reverse("todos-list"), payload, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        todo = Todo.objects.latest("created")
        self.assertEqual(todo.description, "foo description")
        self.assertEqual(todo.tags.count(), 1)

        self.assertEqual(Tag.objects.all().count(), 1)

    def test_todos_api_update_with_tags(self):

        todo = Todo.objects.create(user=self.user, description="foo desc")
        self.assertEqual(todo.tags.count(), 0)

        payload = {
            "description": "foo updated",
            "tags": [{"name": "tag1"}],
        }
        response = self.api.put(
            reverse("todos-detail", args=[todo.number]), payload, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        todo = Todo.objects.get(number=todo.number)
        self.assertEqual(todo.description, "foo updated")
        self.assertEqual(todo.tags.count(), 1)

        payload = {
            "description": "foo updated",
            "tags": [],
        }
        response = self.api.put(
            reverse("todos-detail", args=[todo.number]), payload, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        todo = Todo.objects.get(number=todo.number)
        self.assertEqual(todo.tags.count(), 0)

        # Already existent tag isn't deleted
        self.assertEqual(Tag.objects.all().count(), 1)

        payload = {
            "description": "foo updated",
            "done": True,
            "tags": [{"name": "tag2"}, {"name": "tag1"}],
        }
        response = self.api.put(
            reverse("todos-detail", args=[todo.number]), payload, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        todo = Todo.objects.get(number=todo.number)
        self.assertTrue(todo.done)
        self.assertEqual(todo.tags.count(), 2)
        self.assertEqual(Tag.objects.all().count(), 2)

    def test_todos_api_filter_by_tag_name(self):
        tag1 = Tag.objects.create(user=self.user, name="tag1")
        tag2 = Tag.objects.create(user=self.user, name="tag2")

        Todo.objects.create(user=self.user, description="todo1")

        t1 = Todo.objects.create(user=self.user, description="todo2")
        t1.tags.add(tag1)
        t1.save()

        t2 = Todo.objects.create(user=self.user, description="todo3")
        t2.tags.add(tag1)
        t2.tags.add(tag2)
        t2.save()

        response = self.api.get(reverse("todos-list") + "?tags=tag0")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"], [])

        response = self.api.get(reverse("todos-list") + "?tags=tag1")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 2)

        response = self.api.get(reverse("todos-list") + "?tags=tag2")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)

    def test_todos_api_filter_by_done(self):
        Todo.objects.create(user=self.user, description="todo1", done=True)
        Todo.objects.create(user=self.user, description="todo2", done=False)

        response = self.api.get(reverse("todos-list") + "?done=True")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["description"], "todo1")

        response = self.api.get(reverse("todos-list") + "?done=False")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["description"], "todo2")

        response = self.api.get(reverse("todos-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 2)


class TagViewSetTest(APITestCase, CreateUserMixin):

    def setUp(self):
        self.email = 'test@test.com'
        self.password = 'testpass'
        self.user = self.create_user(email=self.email, password=self.password)
        self.token = self.user.token
        self.api = self.create_authenticated_api_client(self.user.token)

    def test_tags_endpoint_requires_auth(self):
        api = APIClient()
        response = api.post(reverse("tags-list"), {"foo": "foo"}, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        response = api.get(reverse("tags-list"))
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_tags_api_list(self):
        response = self.api.get(reverse("tags-list"), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("count"), 0)
        self.assertEqual(response.data.get("results"), [])

        # Create one tag for current user
        Tag.objects.create(
            user=self.user, name="Tag %s" % self.user.email, color="black")

        response = self.api.get(reverse("tags-list"), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("count"), 1)
        self.assertEqual(len(response.data.get("results")), 1)
        self.assertEqual(
            response.data.get("results")[0]["name"], "Tag %s" % self.user.email)

        # Create one tag for other user, and validate that is not returned
        Tag.objects.create(user=User.objects.create(email="other@test.com"))

        response = self.api.get(reverse("tags-list"), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get("count"), 1)
        self.assertEqual(len(response.data.get("results")), 1)
        self.assertEqual(
            response.data.get("results")[0]["name"], "Tag %s" % self.user.email)

    def test_tags_api_create(self):
        payload = {"name": "important", "color": "blue"}
        response = self.api.post(reverse("tags-list"), payload, format="json")
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_tags_api_detail(self):
        tag = Tag.objects.create(user=self.user, name="tag important")

        response = self.api.get(reverse("tags-detail", args=[tag.id]))
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_tags_api_full_update(self):
        tag = Tag.objects.create(user=self.user, name="tag important")

        # Update a task of current user
        payload = {"name": "tag updated", "color": "red"}
        response = self.api.put(
            reverse("tags-detail", args=[tag.number]), payload, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        tag = Tag.objects.get(number=tag.number)
        self.assertEqual(tag.name, "tag updated")
        self.assertEqual(tag.color, "red")

    def test_tags_api_delete(self):
        tag = Tag.objects.create(user=self.user, name="tag foo")

        # Delete a task of current user
        response = self.api.delete(reverse("tags-detail", args=[tag.number]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            Tag.objects.filter(number=tag.number, user=self.user).count(), 0)
