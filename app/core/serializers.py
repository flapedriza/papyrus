from rest_framework import serializers
from app.users.models import User
from app.core.models import Todo, Project, Tag


class TagSerializer(serializers.ModelSerializer):

    user = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all(),
        default=serializers.CurrentUserDefault(),
        write_only=True,
    )
    number = serializers.IntegerField(read_only=True)

    class Meta:
        model = Tag
        exclude = ("id", "created", "updated")


class ProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project
        exclude = ("created", "updated")


class TodoSerializer(serializers.ModelSerializer):

    user = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.all(),
        default=serializers.CurrentUserDefault(),
        write_only=True,
    )

    number = serializers.IntegerField(read_only=True)
    tags = TagSerializer(many=True, required=False)

    class Meta:
        model = Todo
        exclude = ("id", "created", "updated")

    def create(self, validated_data):
        # Override create method to allow create & assign Tags to a Todo
        # while creating it.

        # Pop tags data from validated_data and create Todo instance
        tags_data = validated_data.pop("tags", [])
        todo = Todo.objects.create(**validated_data)

        # Iterate over recieved tags data and associate each to Todo instance
        for tag_data in tags_data:
            tag, _ = Tag.objects.get_or_create(**tag_data)
            todo.tags.add(tag)

        if tags_data:
            todo.save()

        return todo

    def update(self, instance, validated_data):
        # Override update method to allow update & assign Tags to a Todo
        # while updating it
        tags_data = validated_data.pop("tags", [])

        todo = super(TodoSerializer, self).update(instance, validated_data)

        # Clear existing related tags
        todo.tags.clear()

        # Iterate over recieved tags data and associate each to Todo instance
        for tag_data in tags_data:
            tag, _ = Tag.objects.get_or_create(**tag_data)
            todo.tags.add(tag)

        if tags_data:
            todo.save()
        return todo
