# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-01 11:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20171201_1059'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todo',
            name='number',
            field=models.PositiveIntegerField(db_index=True, default=1),
        ),
    ]
