from django.db import models
from .mixins import TimeStampedModelMixin


class Todo(TimeStampedModelMixin):
    """
    """
    number = models.PositiveIntegerField(default=1, db_index=True)

    user = models.ForeignKey("users.User", related_name="todos")
    project = models.ForeignKey("Project", null=True, blank=True)

    description = models.TextField()
    priority = models.PositiveIntegerField(default=0)

    done = models.BooleanField(default=False)
    deadline = models.DateTimeField(null=True, blank=True)

    tags = models.ManyToManyField("Tag", blank=True)

    class Meta:
        ordering = ("priority", )

    def save(self, *args, **kwargs):
        # On creation auto increment number field if not setted
        if not self.pk:
            if self.user.todos.exists():
                self.number = self.user.todos.latest("number").number + 1
        super(Todo, self).save(*args, **kwargs)


class Tag(TimeStampedModelMixin):
    """
    """
    number = models.PositiveIntegerField(default=1, db_index=True)

    user = models.ForeignKey("users.User", related_name="user_tags")
    name = models.CharField(max_length=255)
    color = models.CharField(max_length=16, null=True, blank=True)

    def __str__(self):  # pragma: no cover
        return "Tag %s" % self.name

    def save(self, *args, **kwargs):
        # On creation auto increment number field if not setted
        if not self.pk:
            if self.user.user_tags.exists():
                self.number = self.user.user_tags.latest("number").number + 1
        super(Tag, self).save(*args, **kwargs)


class Project(TimeStampedModelMixin):
    """
    """
    users = models.ManyToManyField("users.User")
    name = models.CharField(max_length=255)
    color = models.CharField(max_length=16, null=True, blank=True)

    def __str__(self):  # pragma: no cover
        return "Project %s " % self.name
