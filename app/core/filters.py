from django_filters import rest_framework as filters

from app.core.models import Todo


class TodoFilter(filters.FilterSet):
    tags = filters.CharFilter(name="tag", method="filter_by_tag")

    def filter_by_tag(self, queryset, name, value):
        return queryset.filter(tags__name__in=[value])

    class Meta:
        model = Todo
        fields = ("tags", "done")
