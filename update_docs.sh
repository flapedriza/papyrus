#!/usr/bin/env sh
set -u
set -e

echo "Updating docs..."

cd docs/ && make html
