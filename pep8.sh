flake8 --ignore F841 \
    --exclude .git,__pycache__,docs/source/conf.py,migrations,manage.py \
    --max-complexity 10 \
    --max-line-length=90
