# Papyrus API

[![Build Status](https://travis-ci.org/pasqualguerrero/papyrus.svg?branch=master)](https://travis-ci.org/pasqualguerrero/papyrus)

### [Documentation](https://pasqualguerrero.github.io/papyrus/)

### [DockerHub images](https://hub.docker.com/r/pasqualguerrero/papyrus/tags/)


#### Local development

#### Run it
```sh
# Build images
docker-compose build

# Run containers (in background)
docker-compose up -d
```
Api would be available on http://localhost:8001/api/


#### Run tests
```sh
# Install test dependencies for coverage measurement and pep8
docker exec -it papyrus_backend pip install -r requirements_test.txt

# Execute tests
docker exec -it papyrus_backend python run_tests.py
```

#### Before pushing

Run pep8 script
```sh
docker exec -it papyrus_backend sh pep8.sh
```

#### Update the docs

All documentation is generated using Sphinx (1.6.1) and all source files are
placed on `docs/source/` folder.

To preview changes made:

```sh
# Install docs dependencies
docker exec -it papyrus_backend pip install -r requirements_doc.txt
docker exec -it papyrus_backend apk add make

# Execute ./update_docs.sh script on container (this will generate a rendered docs version on `docs/build/html/` folder)
docker exec -it papyrus_backend sh update_docs.sh

# Run a simple http server on `docs/build/html` folder to preview the changes:
cd docs/build/html && python -m SimpleHTTPServer 9002
```
