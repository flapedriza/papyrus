# The Papyrus API


## Authentication

For clients to authenticate, a token key should be included in the Authorization HTTP header.
The key should be prefixed by the string literal "Token", with whitespace separating the two strings.

Example:
```
Authorization: Token db506bd37d6fb344a4d471a2f48bf3ef12577a47
```

To achieve this Papyrus API has two open endpoints described below:


## Todo's

#### POST /api/todos/
Create a new todo

Success response:
  - code: 201
  - content:

```python
    {
      "id": 1,
      "user": "f5658104-23a8-45c3-ab79-133618b9a0de",
      "description": "fake description",
      "priority": 0,
      "done": false,
      "deadline": null,
      "project": null,
      "tags": []
    }
```

Example:
```sh
curl -X POST localhost:8001/api/todos/ -d '{"description": "fake description"}' -H "Content-Type: application/json" -H "Authorization: Token db506bd37d6fb344a4d471a2f48bf3ef12577a47"
```

#### GET /api/todos/
Get a list of all todos

Success response:
  - code: 200
  - content:

```python
    {
      "count": 1,
      "next": null,
      "previous": null,
      "results": [
        {
          "id": 2,
          "user": "f5658104-23a8-45c3-ab79-133618b9a0de",
          "description": "fake description",
          "priority": 0,
          "done": false,
          "deadline": null,
          "project": null,
          "tags": []
        }
      ]
    }
```

Example:
```sh
curl -X GET localhost:8001/api/todos/ -H "Content-Type: application/json" -H "Authorization: Token db506bd37d6fb344a4d471a2f48bf3ef12577a47"
```

#### GET /api/todos/:id/
Get the details of a single todo

Success response:
  - code: 200
  - content:

```python
    {
      "id": 2,
      "user": "f5658104-23a8-45c3-ab79-133618b9a0de",
      "description": "fake description",
      "priority": 0,
      "done": false,
      "deadline": null,
      "project": null,
      "tags": []
    }
```

Example:
```sh
curl -X GET localhost:8001/api/todos/2/ -H "Content-Type: application/json" -H "Authorization: Token db506bd37d6fb344a4d471a2f48bf3ef12577a47"
```

#### PUT /api/todos/:id/ - will update :id todo
Update a single todo

#### PATCH /api/todos/:id/ - will update partially a :id todo
Update partially a single todo

#### DELETE /api/todos/:id/ - will delete :id todo
Delete a single todo

#### POST /api/todos/:id/done/
Mark as done :id todo.

Authenticated: `True`

Success response:
  - code: 200

Example:
```sh
curl -X POST localhost:8001/api/todos/1/done/ -H "Content-Type: application/json" -H "Authorization: Token db506bd37d6fb344a4d471a2f48bf3ef12577a47"
```

#### POST /api/todos/:id/undone/
Mark as undone :id todo.

Authenticated: `True`

Success response:
  - code: 200

Example:
```sh
curl -X POST localhost:8001/api/todos/1/undone/ -H "Content-Type: application/json" -H "Authorization: Token db506bd37d6fb344a4d471a2f48bf3ef12577a47"
```


## Docker

Create dotenv file `.env` on project root with:
```
DOCKER_COMPOSE_ENV=dev
```

```docker-compose build```
```docker-compose up```

Development api will be available on `localhost:8001`

### Run tests

```sh
docker exec -it papyrus_backend pip install -r requirements_test.txt
docker exec -it papyrus_backend python run_tests.py
```

