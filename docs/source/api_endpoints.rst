API Endpoints
=============


Authentication
--------------

For clients to authenticate, a token key should be included in the Authorization HTTP header.
The key should be prefixed by the string literal "Token", with whitespace separating the two strings.

.. code-block:: python

        Authorization: Token db506bd37d6fb344a4d471a2f48bf3ef12577a47

To get the required token a call should be made to register or login endpoints


Register
--------

.. http:post:: /api/register/

    Creates a new user account

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 201 Created
        Vary: Accept
        Content-Type: application/json

        {
            "token": "25693055cfa4cb6b456ba3c22910e1966a82eacd"
        }

    :jsonparam string email: email address for account
    :jsonparam string password: password

    :resheader Content-Type: application/json

    :status 201: A new user is created correctly

    :status 400: Incorrect parameters sent


Login
-----

.. http:post:: /api/login/

    Login into an existent user account

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "token": "25693055cfa4cb6b456ba3c22910e1966a82eacd"
        }

    :jsonparam string username: email address for account
    :jsonparam string password: password

    :resheader Content-Type: application/json

    :status 200: Succesfully logged in

    :status 400: Incorrect login


Todo's
-------

.. http:post:: /api/todos/:number/

  TBD

.. http:get:: /api/todos/

    Get a list of all todos

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
          "count": 1,
          "next": null,
          "previous": null,
          "results": [
            {
              "number": 2,
              "description": "fake description",
              "priority": 0,
              "done": false,
              "deadline": null,
              "project": null,
              "tags": [{"name": "docs", "color": "red"}]
            }
          ]
        }

    :query tags: filter by tag name `?tags=docs`
    :query done: filter todo's by done/undone `?done=True or ?done=False`

    :reqheader Authorization: Token <valid_token_key>

    :resheader Content-Type: application/json

    :status 200: OK

.. http:get:: /api/todos/:number/

  TBD
.. http:put:: /api/todos/:number/

  TBD
.. http:patch:: /api/todos/:number/

  TBD
.. http:delete:: /api/todos/:number/

  TBD

Tags
----

.. http:get:: /api/tags/

    Get a list of all tags

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
          "count": 1,
          "next": null,
          "previous": null,
          "results": [
            {
              "number": 1,
              "name": "docs",
              "color": "red"
            },
            {
              "number": 2,
              "name": "important",
              "color": null
            },
          ]
        }

    :reqheader Authorization: Token <valid_token_key>

    :resheader Content-Type: application/json

    :status 200: OK

.. http:put:: /api/tags/:id/

  TBD
.. http:delete:: /api/tags/:id/

  TBD
