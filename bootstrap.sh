#!/usr/bin/env sh
set -u
set -e

echo "Waiting for postgres..."
until nc -z ${POSTGRES_HOST} ${POSTGRES_PORT}
do
  sleep 1
done
sleep 1

./manage.py migrate --noinput
./manage.py collectstatic --noinput

if [ "${ENVIRON:-dev}" = "dev" ]
then
  ./manage.py runserver "0.0.0.0:8000"
else
  gunicorn -w 1 --log-level debug -b "0.0.0.0:8000" -t 60 wsgi:application
fi
