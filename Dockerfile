FROM python:3.6.1-alpine

RUN apk update
RUN apk add --no-cache gcc git python3-dev musl-dev postgresql-dev

WORKDIR /code

RUN pip install --upgrade pip
ADD requirements.txt /code/
RUN pip install --no-cache-dir -r requirements.txt

ADD ["bootstrap.sh", "manage.py", "/code/"]
ADD app /code/app/
ADD papyrus /code/papyrus/

VOLUME '/static'

CMD ["/bin/sh", "bootstrap.sh"]

EXPOSE 8000
